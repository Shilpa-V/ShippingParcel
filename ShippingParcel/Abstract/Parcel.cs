﻿using ShippingParcel.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShippingParcel

{
    /// <summary>
    /// Parcel - Abstarct Base Class
    /// </summary>
    public abstract class Parcel : IParcel, IParcelCategory
    {
        public uint Weight { get; set; }
        public uint Height { get; set; }
        public uint Width { get; set; }
        public uint Depth { get; set; }

        //Defualt Constructor
        public Parcel() { }

        //Parameterized Constructor
        public Parcel(uint weight, uint height, uint width, uint depth)
        {
            this.Weight = weight;
            this.Height = height;
            this.Width = width;
            this.Depth = depth;
        }

        //To Calculate Shipping Cost
        public abstract decimal CaluclateShippingCost(uint weight, uint volume);

        //To Get ParcelCategory
        public abstract string ParcelCategory();
              
        //Virtaul method - To Calculate Volume
        public virtual uint CalculateVolume(uint height, uint width, uint depth)
        {
            return height * width * depth;
        }
    }
}

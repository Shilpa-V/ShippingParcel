﻿using ShippingParcel.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShippingParcel
{
    class Program
    {
        static void Main(string[] args)
        {
            // local varibales declaration
            uint weight, height, width, depth;
            decimal shippingCost;
            string parcelCategory;
            string choice;

            // Loop throught until user continues
            do
            {
                //Clear Console 
                Console.Clear();

                // Input integers to intialize weight, height, width, depth
                InputData(out weight, out height, out width, out depth);

                //Calcualte shipping cost as per the size of parcel and intitalize parcel category accordingly
                shippingCost = CalculateShippingCost(weight, height, width, depth, out parcelCategory);

                // Print shipping cost and parcel category on console
                OutputShippingData(parcelCategory, shippingCost);

                //Ask user to continue?
                Console.Write("Enter (y/yes) to continue:");
                choice = Console.ReadLine();

            } while (choice.ToLower() == "yes" || choice.ToLower() == "y");
                      
        }
              
        // Initialize and validate weight, height, width and depth
        private static void InputData(out uint weight, out uint height, out uint width, out uint depth )
        {
            //Read Weight
            Console.Write("Enter Weight in kg:");
            string parcelWeight = Console.ReadLine();

            //Validate Weight
            Validate(parcelWeight, out weight, "Weight", "Kg");
           
            //Read Height
            Console.Write("Enter Height in cm:");
            string parcelHeight = Console.ReadLine();

            //Validate Height
            Validate(parcelHeight, out height, "Height", "Cm");

            //Read Width
            Console.Write("Enter Width in cm:");
            string parcelWidth = Console.ReadLine();

            //Validate Width
            Validate(parcelWidth, out width, "Width", "Cm");

            //Read Depth
            Console.Write("Enter Depth in cm:");
            string parcelDepth = Console.ReadLine();

            //Validate Depth
            Validate(parcelDepth, out depth, "Depth", "Cm");

        }

        //Validate value for Postivie Integer and should not be zero
        private static void Validate(string inputValue, out uint validateValue, string valueType, string valueUnit)
        {

            while (!UInt32.TryParse(inputValue, out validateValue) || validateValue == 0)
            {
                Console.WriteLine("Not a valid {0}, try again.(Positive Integer/ Not Zero)", valueType);
                Console.Write("Enter {0} in {1}:", valueType, valueUnit);
                inputValue = Console.ReadLine();
            }
            
        }

        // Calculate shipping cost and initialize parcel category
        private static decimal CalculateShippingCost(uint weight, uint height, uint width, uint depth, out string parcelCategory)
        {
            HeavyParcel heavyParcel = new HeavyParcel(weight, height, width, depth);
            uint volume = heavyParcel.CalculateVolume(height, width, depth);

            if(weight > 50)
            {
                RejectParcel rejectParcel = new RejectParcel();
                parcelCategory = rejectParcel.ParcelCategory();
                return 0m; //string to decimal
            }
            else if (weight > 10)
            {
                parcelCategory = heavyParcel.ParcelCategory();
                return heavyParcel.CaluclateShippingCost(weight, volume);
            }
            else if(volume < 1500 )
            {
                SmallParcel smallParcel = new SmallParcel(weight, height, width, depth);
                parcelCategory = smallParcel.ParcelCategory();
                return smallParcel.CaluclateShippingCost(weight, volume);
            }
            else if(volume < 2500)
            {
                MediumParcel mediumParcel = new MediumParcel(weight, height, width, depth);
                parcelCategory = mediumParcel.ParcelCategory();
                return mediumParcel.CaluclateShippingCost(weight, volume);
            }
            else
            {
                LargeParcel largeParcel = new LargeParcel(weight, height, width, depth);
                parcelCategory = largeParcel.ParcelCategory();
                return largeParcel.CaluclateShippingCost(weight, volume);
            }

           
        }

        // Print shipping cost and parcel category on console screen
        private static void OutputShippingData(string category, decimal cost)
        {
            Console.WriteLine();
            Console.WriteLine("Category : {0}", category);
            Console.WriteLine();
            if (cost == 0)
            {
                Console.WriteLine("Cost : N/A", cost);
                Console.WriteLine("Only Accept Parcels with weight <= 50 Kg");
            }
            else
                Console.WriteLine("Cost : ${0}", cost);
        }
    }
}

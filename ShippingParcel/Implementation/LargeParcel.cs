﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShippingParcel.Implementation
{
    /// <summary>
    /// LargeParcel - child class
    /// </summary>
    public class LargeParcel : Parcel
    {

        //Defualt Constructor
        public LargeParcel()
        { }

        //Parameterized Constructor - calls base class constructor
        public LargeParcel(uint weight, uint height, uint width, uint depth) : base(weight, height, width, depth)
        { }

        //Implementation to calcualte shipping cost
        public override decimal CaluclateShippingCost(uint weight, uint volume)
        {
            return 0.03m * volume;

        }

        //Implementation to get ParcelCategory
        public override string ParcelCategory()
        {
            return "Large Parcel"; 
        }
    }
}

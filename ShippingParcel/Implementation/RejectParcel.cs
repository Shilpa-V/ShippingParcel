﻿using ShippingParcel.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShippingParcel.Implementation
{

    /// <summary>
    /// RejectParcel 
    /// </summary>
    public class RejectParcel : IParcelCategory
    {
        //Defualt Construcotr
        public RejectParcel()
        { }

        //Implementation to get ParcelCategory
        public string ParcelCategory()
        {
            return "Rejected";
        }
    }
}

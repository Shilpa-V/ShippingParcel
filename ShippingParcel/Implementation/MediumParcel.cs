﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShippingParcel.Implementation
{
    /// <summary>
    /// MediumParcel - child class
    /// </summary>
    public class MediumParcel : Parcel
    {
        //Defualt Constructor
        public MediumParcel()
        { }

        //Parameterized Constructor - calls base class constructor
        public MediumParcel(uint weight, uint height, uint width, uint depth) : base(weight, height, width, depth)
        { }

        //Implementation to calcualte shipping cost
        public override decimal CaluclateShippingCost(uint weight, uint volume)
        {
            return 0.04m * volume;
        }

        //Implementation to get ParcelCategory
        public override string ParcelCategory()
        {
            return "Medium Parcel";
        }
    }
}

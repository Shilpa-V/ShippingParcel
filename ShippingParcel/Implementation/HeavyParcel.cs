﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShippingParcel.Implementation
{
    /// <summary>
    /// HeavyParcel - child class
    /// </summary>
    public class HeavyParcel : Parcel
    {
        //Defualt Constructor
        public HeavyParcel()
        { }

        //Parameterized Constructor - calls base class constructor
        public HeavyParcel(uint weight, uint height, uint width, uint depth): base(weight, height, width, depth)
        { }

        //Implementation to calcualte shipping cost
        public override decimal CaluclateShippingCost(uint weight, uint volume)
        {
            return weight * 15m;
        }

        //Implementation to get ParcelCategory
        public override string ParcelCategory()
        {
            return "Heavy Parcel";
        }
    }
}

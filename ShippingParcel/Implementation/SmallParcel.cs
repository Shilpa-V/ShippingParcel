﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShippingParcel.Implementation
{
    /// <summary>
    /// SmallParcel - child class
    /// </summary>
    public class SmallParcel : Parcel
    {
        //Defualt Constructor
        public SmallParcel()
        { }

        //Parameterized Constructor - calls base class constructor
        public SmallParcel(uint weight, uint height, uint width, uint depth) : base(weight, height, width, depth)
        { }

        //Implementation to calcualte shipping cost
        public override decimal CaluclateShippingCost(uint weight, uint volume)
        {
            return  0.05m * volume;
        }

        //Implementation to get ParcelCategory
        public override string ParcelCategory()
        {
            return "Small Parcel";
        }
    }
}

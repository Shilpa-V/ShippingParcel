﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShippingParcel.Interface
{
    interface IParcel
    {
        decimal CaluclateShippingCost(uint weight, uint volume);
        uint CalculateVolume(uint height, uint width, uint depth);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using ShippingParcel.Implementation;

namespace ShippingParcel.Test
{
    [TestClass]
    public class ParcelTest
    {
        
        public string ParcelJson;
        public dynamic ParcelData;
        
        [TestInitialize]
        public void TestInitialize()
        {
            ParcelJson = File.ReadAllText("SampleData/parcels.json");
            ParcelData = JsonConvert.DeserializeObject(ParcelJson);

        }

        [TestMethod]
        public void TestHeavyParcel()
        {
            
                
            //Arrange
            HeavyParcel heavyParcel = new HeavyParcel();
            decimal expectedCost = 15m * Convert.ToDecimal(ParcelData[1].weight) ;
            string expectedCategory = ParcelData[1].category;
            
            //Act
            decimal actualCost = heavyParcel.CaluclateShippingCost(22, 0);
            string actualCategory = heavyParcel.ParcelCategory();


            //Assert
            Assert.AreEqual(expectedCost, actualCost);
            Assert.AreEqual(expectedCategory, actualCategory);

        }

        [TestMethod]
        public void TestSmallParcel()
        {
            //Arrange
            SmallParcel smallParcel = new SmallParcel();

            uint expectedVolume = Convert.ToUInt32(ParcelData[2].height) * Convert.ToUInt32(ParcelData[2].width) * Convert.ToUInt32(ParcelData[2].depth);
            decimal expectedCost = 0.05m * Convert.ToDecimal(expectedVolume);
            string expectedCategory = ParcelData[2].category;

            //Act
            uint actualVolume = smallParcel.CalculateVolume(Convert.ToUInt32(ParcelData[2].height), Convert.ToUInt32(ParcelData[2].width), Convert.ToUInt32(ParcelData[2].depth));
            decimal actualCost = smallParcel.CaluclateShippingCost(Convert.ToUInt32(ParcelData[2].weight), actualVolume);
            string actualCategory = smallParcel.ParcelCategory();


            //Assert
            Assert.AreEqual(expectedVolume, actualVolume);
            Assert.AreEqual(expectedCost, actualCost);
            Assert.AreEqual(expectedCategory, actualCategory);
        }

        [TestMethod]
        public void TestMediumParcel()
        {
            //Arrange
            MediumParcel mediumParcel = new MediumParcel();

            uint expectedVolume = Convert.ToUInt32(ParcelData[0].height) * Convert.ToUInt32(ParcelData[0].width) * Convert.ToUInt32(ParcelData[0].depth);
            decimal expectedCost = 0.04m * Convert.ToDecimal(expectedVolume);
            string expectedCategory = ParcelData[0].category;

            //Act
            uint actualVolume = mediumParcel.CalculateVolume(Convert.ToUInt32(ParcelData[0].height), Convert.ToUInt32(ParcelData[0].width), Convert.ToUInt32(ParcelData[0].depth));
            decimal actualCost = mediumParcel.CaluclateShippingCost(Convert.ToUInt32(ParcelData[0].weight), actualVolume);
            string actualCategory = mediumParcel.ParcelCategory();


            //Assert
            Assert.AreEqual(expectedVolume, actualVolume);
            Assert.AreEqual(expectedCost, actualCost);
            Assert.AreEqual(expectedCategory, actualCategory);
        }

        [TestMethod]
        public void TestLargeParcel()
        {
            //Arrange
            LargeParcel largeParcel = new LargeParcel();

            uint expectedVolume = Convert.ToUInt32(ParcelData[4].height) * Convert.ToUInt32(ParcelData[4].width) * Convert.ToUInt32(ParcelData[4].depth);
            decimal expectedCost = 0.03m * Convert.ToDecimal(expectedVolume);
            string expectedCategory = ParcelData[4].category;

            //Act
            uint actualVolume = largeParcel.CalculateVolume(Convert.ToUInt32(ParcelData[4].height), Convert.ToUInt32(ParcelData[4].width), Convert.ToUInt32(ParcelData[4].depth));
            decimal actualCost = largeParcel.CaluclateShippingCost(Convert.ToUInt32(ParcelData[4].weight), actualVolume);
            string actualCategory = largeParcel.ParcelCategory();

            //Assert
            Assert.AreEqual(expectedVolume, actualVolume);
            Assert.AreEqual(expectedCost, actualCost);
            Assert.AreEqual(expectedCategory, actualCategory);
        }

        [TestMethod]
        public void TestRejectParcel()
        {
            //Arrange
            RejectParcel largeParcel = new RejectParcel();

            uint actualWeight = ParcelData[3].weight;
            string expectedCategory="";

            //Act
            if(actualWeight > 50)
                expectedCategory = ParcelData[3].category;
            string actualCategory = largeParcel.ParcelCategory();


            //Assert
            Assert.AreEqual(expectedCategory, actualCategory);
        }
    }
}
